#ifndef FLOATCOMP_HEAD
#define FLOATCOMP_HEAD

#pragma once
#include "IntCompressor.hpp";
namespace uqac::serialization
{
	class FloatCompressor
	{
	public:

		FloatCompressor() = default;
		FloatCompressor(float, float, int);
		~FloatCompressor() = default;

		boost::dynamic_bitset<> Compress(double value);
		double Decompress(boost::dynamic_bitset<>);

	private:

		IntCompressor IntCom;
		float min, max;
		float precision;

	};
}

#endif