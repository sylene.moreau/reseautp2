#pragma once

#include <vector>
#include <cstdint>
#include "framework.h"


namespace uqac::game 
{

	class Player 
	{
	public:
			Player() = default;
			~Player() = default;

			void DisplayPlayerInfo();
			//---------------SETTERS------------------
			void setposition(Vector3 pos) {
				position = pos;
			}

			void setTaille(Vector3 scale) {
				taille = scale;
			}
			void setRotation(Quaternion rot) {
				rotation = rot;
			}
			 void setVie(uint16_t health) {vie = health;}
			 void setArmure(uint8_t armor) {armure = armor;}
			 void setArgent(float monney) {argent = monney;}
			 void setNom(char* name) {nom = name;}

			//-------------GETTERS----------------
			 float getPositionX() { return position.x;}
			 float getPositionY() { return position.y; }
			 float getPositionZ() { return position.z; }

			 Quaternion getRotation() { return rotation; }
			 float getRotationX() { return rotation.x; }
			 float getRotationY() { return rotation.y; }
			 float getRotationZ() { return rotation.z; }
			 float getRotationW() { return rotation.w; }

			 float getTailleX() { return taille.x; }
			 float getTailleY() { return taille.y; }
			 float getTailleZ() { return taille.z; }

			 uint16_t getVie() { return vie; }
			 char  getArmure() { return armure; }
			 float getArgent() { return argent; }
			 char* getNom() { return nom; }

		private:
			Vector3 position = {0,0,0};
			Quaternion rotation = {0,0,0,1};
			Vector3 taille = {0,0,0};
			uint16_t vie=300;
			uint8_t armure=50;
			float argent=0.00;
			char* nom="";
	};
}
