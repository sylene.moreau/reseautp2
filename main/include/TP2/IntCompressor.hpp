#ifndef INTCOMP_HEAD
#define INTCOMP_HEAD

#pragma once
#include <boost/dynamic_bitset.hpp>
namespace uqac::serialization
{
	class IntCompressor
	{
	public:

		IntCompressor() = default;
		IntCompressor(int, int);
		~IntCompressor() = default;
		boost::dynamic_bitset<> Compress(int value);
		int Decompress(boost::dynamic_bitset<>);

	private:
		int min, max;

		

		
	};
}
#endif