#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <boost/dynamic_bitset.hpp>

namespace uqac::serialization
{
	class Deserializer
	{
	public:
		Deserializer(const uint8_t* buffer, size_t bufferSize);
		Deserializer()=default;
		~Deserializer()=default;

		uint32_t readBytes();
		char *readText();

		boost::dynamic_bitset<> Deserialize();

		
		

	private:
		const uint8_t* mBuffer;
		size_t mBufferSize;
		size_t mBytesRead{ 0 };
	};
}