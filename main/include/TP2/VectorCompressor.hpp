#ifndef VECTCOMP_HEAD
#define VECTCOMP_HEAD

#pragma once
#include <boost/dynamic_bitset.hpp>
#include "FloatCompressor.hpp";
namespace uqac::serialization
{
	

	class VectorCompressor
	{
	public:
		VectorCompressor() = default;
		VectorCompressor(float, float, float, float, float, float, int);
		~VectorCompressor() = default;

		inline int GetBytesNeededForX() { return(CompX.GetBytesNeeded()); };
		inline int GetBytesNeededForY() { return(CompY.GetBytesNeeded()); };
		inline int GetBytesNeededForZ() { return(CompZ.GetBytesNeeded()); };

		std::vector<boost::dynamic_bitset<>> Compress(Vector3 values);
		Vector3 Decompress(std::vector<boost::dynamic_bitset<>> bits);

		

	private:
		float xmin, xmax, ymin, ymax, zmin, zmax;
		int precision;
		FloatCompressor CompX;
		FloatCompressor CompY;
			FloatCompressor CompZ;
	};
}
#endif