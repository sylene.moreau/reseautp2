#pragma once

#include <cstdint>
#include <iostream>
#include <string>
#include <vector>
#include <boost/dynamic_bitset.hpp>

namespace uqac::serialization {

    class Serializer {
        public:
            Serializer() = default;
            ~Serializer() = default;

            template <typename Type>
            bool Write(Type data);

            bool writeBytes( char[], size_t);
            bool writeText(char * );
            void printBuffer();

            bool Serialize(boost::dynamic_bitset<> bits);

            inline int getBufferSize() const { return container.size(); } 
            inline const uint8_t* getBufferData() const { return container.data(); }
            

        private:
            std::vector<uint8_t> container;
            int position=0;
    };

    template <typename Type>
    bool Serializer::Write(Type data) {

        char serializable[sizeof(data)];

        std::copy(static_cast<const char*>(static_cast<const void*>(&data)),
            static_cast<const char*>(static_cast<const void*>(&data)) + sizeof(data),
            serializable);

        return writeBytes(serializable, sizeof(Type));
    }

}
