#ifndef QUACOMP_HEAD
#define QUACOMP_HEAD

#pragma once
#include<vector>
#include <boost/dynamic_bitset.hpp>

#include "framework.h"

namespace uqac::serialization
{

	class QuaternionCompressor
	{
	public:
		QuaternionCompressor() = default;
		~QuaternionCompressor() = default;

		boost::dynamic_bitset<> Compress(Quaternion value);
		Quaternion Decompress(boost::dynamic_bitset<> value);
	};

}
#endif