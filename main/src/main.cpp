#include "player.hpp"
#include <iostream>
#include "Serializer.hpp"
#include "Deserializer.hpp"
#include "IntCompressor.hpp"
#include "FloatCompressor.hpp"
#include "QuaternionCompressor.hpp"
#include "framework.h"
int main()
{
	//--------------SERIALIZER-----------------------
	uqac::serialization::Serializer S;

	uqac::game::Player POriginal;
	POriginal.setRotation(Quaternion{ 0.645,-0.387,-0.051,-0.657 });
	POriginal.setArgent(88800.3);
	POriginal.setArmure(20);
	POriginal.setNom("Sylene");
	POriginal.setposition(Vector3{ -262.152, 0.135, 50.236 });
	POriginal.setTaille(Vector3{ 1, 1, 1 });
	POriginal.setVie(253);

	

	uqac::serialization::FloatCompressor posXY(-500.00, 500.00,3);
	uqac::serialization::FloatCompressor posZ(0.00, 100.00,3);
	uqac::serialization::FloatCompressor argent(-99999.99, 99999.99,2);
	uqac::serialization::QuaternionCompressor rotation;
	uqac::serialization::FloatCompressor scale(1.0, 10.0,3);
	uqac::serialization::IntCompressor vie(0, 300);
	uqac::serialization::IntCompressor armure(0, 50);

	S.Serialize(posXY.Compress(POriginal.getPositionX()));
	S.Serialize(posXY.Compress(POriginal.getPositionY()));
	S.Serialize(posZ.Compress(POriginal.getPositionZ()));

	S.Serialize(argent.Compress(POriginal.getArgent()));

	S.Serialize(rotation.Compress(POriginal.getRotation()));

	S.Serialize(scale.Compress(POriginal.getTailleX()));
	S.Serialize(scale.Compress(POriginal.getTailleY()));
	S.Serialize(scale.Compress(POriginal.getTailleZ()));

	S.Serialize(vie.Compress(POriginal.getVie()));

	S.Serialize(armure.Compress(POriginal.getArmure()));

	S.writeText(POriginal.getNom());

	//-----------------------DESERIALIZER--------------------------------
	
	uqac::game::Player PCopy;
	uqac::serialization::Deserializer D(S.getBufferData(),S.getBufferSize());
	
	Vector3 pos{
		posXY.Decompress(D.Deserialize()),
		posXY.Decompress(D.Deserialize()),
		posZ.Decompress(D.Deserialize())
	};
	PCopy.setposition(pos);

	PCopy.setArgent(argent.Decompress(D.Deserialize()));
	
	PCopy.setRotation(rotation.Decompress(D.Deserialize()));
	
	Vector3 sca{
		scale.Decompress(D.Deserialize()),
		scale.Decompress(D.Deserialize()),
		scale.Decompress(D.Deserialize())
	};
	PCopy.setTaille(sca);
	
	PCopy.setVie(vie.Decompress(D.Deserialize()));

	PCopy.setArmure(armure.Decompress(D.Deserialize()));

	
	PCopy.setNom(D.readText());
	
	//--------------------AFFICHAGE--------------
	std::cout << "Joueur Original : \n";
	POriginal.DisplayPlayerInfo();
	std::cout << "\n\nJoueur Deserialise : \n";
	PCopy.DisplayPlayerInfo();

	


	

	
	
	


}