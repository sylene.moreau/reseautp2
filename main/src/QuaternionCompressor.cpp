#include "QuaternionCompressor.hpp"
#include<string>
#include<iostream>
#include<bitset>

namespace uqac::serialization
{
	boost::dynamic_bitset<> QuaternionCompressor::Compress(Quaternion value)
	{
		std::vector<float> vals   // on met dans un vecteur pour pouvoir iterer facilement dessus
		{
			value.x,
			value.y, 
			value.z, 
			value.w
		}; 

		std::string c;
		float max = -1;
		int index = 0;
		for (int i = 0; i < 4; i++) //on cherche a valeur max du quaternion pour coder sa position
			if (vals[i] > max)
			{
				max = vals[i];
				index = i;
			}

		for (int i = 0; i < 4; i++)
		{
			float v = vals[i];
			if (i != index)
			{
				if (v > 0.707) v = 0.707;    
				if (v < -0.707) v = -0.707;
				v = v * 1000;			//on enleve la virgule
				v += 707;				//on remet en positif
				if (v >= 1024) v = 1023; //on s'autorise a perdre de la data
				if (v < 0) v = 0;		//verification au cas ou
				c += std::bitset<10>((int)v).to_string(); //on ajoute la suite de 10 bit dans la chaine
			}
		}
		c += std::bitset<2>(index).to_string(); // on predn les 2 bits restand pour coder la position manquante

		return boost::dynamic_bitset<>(c);
	}

	Quaternion QuaternionCompressor::Decompress(boost::dynamic_bitset<> value)
	{
		std::string st;
		boost::to_string(value,st); // on converti la chaine de bit en string...

		std::string missingIndex = st.substr(30, 2); //on recuperer les 2 dernier bits pour la position
		std::string first = st.substr(0, 10);
		std::string second = st.substr(10, 10);   // et les 3 autres valeurs du quaternion
		std::string third = st.substr(20, 10);


		std::vector<unsigned long> Elements
		{ 
			std::bitset<10>(first).to_ulong(),
			std::bitset<10>(second).to_ulong(),
			std::bitset<10>(third).to_ulong() 
		};

		
		std::vector<float> quater; //pour stocker les elements decompresses
		for (auto e : Elements)
		{
			float temp=e;
			temp -= 707;
			temp = temp / 1000;
			quater.push_back(temp);
		}

		float missingElem = sqrt(1 - (pow(quater[0], 2) + pow(quater[1], 2) + pow(quater[2], 2)));
		auto it = quater.insert(quater.begin() + std::bitset<10>(missingIndex).to_ulong(), missingElem);
		Quaternion result{ quater[0] ,quater[1] ,quater[2] ,quater[3] };
		return result;
	}
}
