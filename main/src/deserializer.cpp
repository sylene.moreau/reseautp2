#include "deserializer.hpp"

namespace uqac::serialization
{
	Deserializer::Deserializer(const uint8_t* buffer, size_t bufferSize)
	{
		mBuffer = buffer;
		mBufferSize = bufferSize;
	}

	uint32_t Deserializer::readBytes() // lit un element (taille + data correpsondante) dans le buffer
	{
		int size = mBuffer[mBytesRead];
		mBytesRead++;

		char temp[4] = "";
		
		int i = 0;
		for (int j = mBytesRead; j < mBytesRead + size; j++)
		{
			temp[i] = mBuffer[j]; i++;
		}
		mBytesRead += size;
		uint32_t valeur;
		memcpy(&valeur, temp, sizeof(valeur));
		return valeur;
	}

	char* Deserializer::readText() //lit un element texte dans le buffer, meme probleme nous n'avons pas su les mettre ensemble
	{
		int size = mBuffer[mBytesRead];
		mBytesRead++;
		std::string s;
		int i = 0;
		for (int j = mBytesRead; j < mBytesRead + size; j++)
		{
			s.push_back(mBuffer[j]);
			i++;
		}
		char* text = new char[s.length() + 1];
		strcpy(text, s.c_str());
		mBytesRead += size;
		return text;
	}

	boost::dynamic_bitset<> Deserializer::Deserialize()
	{
		uint32_t val = readBytes();
		boost::dynamic_bitset temp;
		temp.append(val);

		return temp;
	}

}