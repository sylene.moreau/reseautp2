#include "player.hpp"
#include <iostream>

namespace uqac::game
{
	void Player::DisplayPlayerInfo()
	{
		std::cout << "\nINFO : \n";
		std::cout << "Nom : " << getNom() << std::endl;
		std::cout << "Vie : " << getVie() << std::endl;
		std::cout << "Armure : " << getArmure() << std::endl;
		std::cout << "Argent : " << getArgent() << std::endl;
		
		std::cout << "Rotation : x " << getRotationX();
		std::cout << ", y " << getRotationY();
		std::cout << ", z " << getRotationZ();
		std::cout << ", w " << getRotationW() << std::endl;

		std::cout << "Position : x " << getPositionX();
		std::cout << ", y " << getPositionY();
		std::cout << ", z " << getPositionZ() << std::endl;

		std::cout << "Taille : longueur " << getTailleX();
		std::cout << ", largeur " << getTailleY();
		std::cout << ", hauteur " << getTailleZ() << std::endl;
	}
}