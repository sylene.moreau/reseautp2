#include<bitset>
#include<iostream>
#include<string>
#include"IntCompressor.hpp"

namespace uqac::serialization
{
	IntCompressor::IntCompressor(int mi, int ma)
	{
		min = mi;
		max = ma;
	}


	boost::dynamic_bitset<> IntCompressor::Compress(int value)
	{
		auto val = value -min;

		if (value > max || value < min)
		{
			std::cout << "la valeur a compresser n'est pas dans la range du compresseur" << std::endl;
		}

		// en prenant en compte le vrai nombre de bit necesaaire a coder l'info et non pas avec la range
		//j'ai trouve plus optimal en place de coder avec le nb de bit necessaire pour coder la valeur elle meme et non pas sa range
		boost::dynamic_bitset bits;
		bits.append(val);
		std::string str;
		boost::to_string(bits, str);
		str.erase(0, str.find_first_not_of('0'));
	
		
		if (str.length() <= 32) //uint 32 - 4 octets
		{
			if (str.length() <= 16) //uint 16 - 2 octets
			{
				if (str.length() <= 8)	//uint 8 - 1 octets
				{
					bits.append((uint8_t)val);
					bits.resize(8);
				}
				else
				{
					bits.append((uint16_t)val);
					bits.resize(16);
				}
			}
			else
			{
				bits.append((uint32_t)val);
				bits.resize(32);
			}
		}
		else //uint 64 - 8 octets
		{
			bits.append((uint64_t)val);
			bits.resize(64);
		}
		
		return(bits);
	}

	int IntCompressor::Decompress(boost::dynamic_bitset<> value)
	{
		return value.to_ulong() + min;

	}
}