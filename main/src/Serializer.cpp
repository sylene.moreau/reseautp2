#include "Serializer.hpp"


namespace uqac::serialization {


    bool Serializer::writeBytes( char buffer[], size_t nbOctet) //ecrit les octets dans le buffer
    {
        container.resize(container.size() + nbOctet+1);
        container[position] = (uint8_t)nbOctet;
        position++;
        int i = 0;
        for (position; position < container.size(); position++)
        {
            container[position] = buffer[i];
            i++;
        }
        return true;
    }
    
    bool Serializer::writeText(char* text) //ecrit un texte dans le buffer, nous avons pas reussi a mettre les 2 en commun
    {
        int nbOctet = strlen(text);
        container.resize(container.size() + nbOctet + 1);
        container[position] = (uint8_t)nbOctet;
        position++;
        int j = 0;
	    for (position; position < container.size(); position++)
	    {
		    container[position] = text[j];
		    j++;
	    }
	    return true;
    }

    void Serializer::printBuffer() //affiche le buffer
    {
        std::cout << "myvector contains:";

        for (auto it = container.cbegin(); it != container.cend(); ++it)
            std::cout << ' ' << *it;
        std::cout << '\n';
    }


    bool Serializer::Serialize(boost::dynamic_bitset<> compressedBits) //a utiliser avec les compresseur. permet d'ecrire avec la bonne taille
    {
        auto val = compressedBits.size();
        if (val <= 32) //uint 32 4
            if (val <= 16) //uint 16 2
                if (val <= 8)	//uint 8 1
                    return Write<uint8_t>((uint8_t)compressedBits.to_ulong());
                else
                    return Write<uint16_t>((uint16_t)compressedBits.to_ulong());
            else
                return Write<uint32_t>((uint32_t)compressedBits.to_ulong());
        else
            return Write<uint64_t>((uint64_t)compressedBits.to_ulong());

    }

}

