#include "VectorCompressor.hpp"
#include <iostream>
#include <bitset>
#include <sstream>
namespace uqac::serialization
{
	
	VectorCompressor::VectorCompressor(float xmi, float xma, float ymi, float yma, float zmi, float zma, int prec)
	{
		xmin = xmi;
		xmax = xma;
		ymin = ymi; 
		ymax = yma;
		zmin = zmi;
		zmax = zma;
		precision = prec;
		CompX= FloatCompressor(xmin, xmax, precision);
		CompY= FloatCompressor(ymin, ymax, precision);
		CompZ= FloatCompressor(zmin, zmax, precision);
	}

	std::vector<boost::dynamic_bitset<>> VectorCompressor::Compress(Vector3 values)
	{
		boost::dynamic_bitset bitsX = CompX.Compress(values.x);
		std::cout << bitsX << std::endl;

		boost::dynamic_bitset bitsY = CompY.Compress(values.y);
		std::cout << bitsY << std::endl;

		boost::dynamic_bitset bitsZ = CompZ.Compress(values.z);
		std::cout << bitsZ << std::endl;

		std::vector<boost::dynamic_bitset<>> bits;
		bits.push_back(bitsX);
		bits.push_back(bitsY);
		bits.push_back(bitsZ);
		return bits;
	}

	Vector3 VectorCompressor::Decompress(std::vector<boost::dynamic_bitset<>> bits)
	{
		boost::dynamic_bitset bitsX =bits[0];
		boost::dynamic_bitset bitsY= bits[1];
		boost::dynamic_bitset bitsZ= bits[2];
		std::cout << "x compressed " << bits[0].to_ulong() << std::endl;
		std::cout << "y compressed " << bits[1].to_ulong() << std::endl;
		std::cout << "z compressed " << bits[2].to_ulong() << std::endl;

		Vector3 result;
		result.x = CompX.Decompress(bitsX);
		result.y = CompY.Decompress(bitsY);
		result.z = CompZ.Decompress(bitsZ);
		std::cout << "x decompressed " << result.x << std::endl;
		std::cout << "y decompressed " << result.y << std::endl;
		std::cout << "z decompressed " << result.z << std::endl;

		return result;
	}

}
