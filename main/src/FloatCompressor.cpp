#include"FloatCompressor.hpp"
#include <iostream>
#include<iomanip>

namespace uqac::serialization
{
	FloatCompressor::FloatCompressor(float mi, float ma, int prec)
	{
		min = mi;
		max = ma;
		precision = prec;
		IntCom = IntCompressor(min * pow(10, precision), max * pow(10, precision));
	}

	boost::dynamic_bitset<> FloatCompressor::Compress(double value)
	{
		boost::dynamic_bitset bits = IntCom.Compress(value * pow(10, precision)); //on compresse la valeur entiere du float avec le degree de precision
		return(bits);
	}
	double FloatCompressor::Decompress(boost::dynamic_bitset<> value)
	{
		return (IntCom.Decompress(value) / pow(10, precision));
	}

}