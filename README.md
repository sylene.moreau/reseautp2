# ReseauTP2

## Différences Byte / Text

Nous avons eu du mal à gérer la différence entre ecrire/lire une chaîne de caractères et un type de base comme les unit, c'est pour cela qu l'on a 2 fonctions write et read.
Cela est probablement dû à la spécialité des compresseurs et de leur retour et qu'il n'y avait pas de compresseurs pour les chaînes de caractères.

## Compresseurs

N'ayant pas vraiment saisi l'utilité de la range maximum alors qu'on peut compresser sur la taille de la valeur elle même directement, les compresseurs n'ont pas d'attributs max_range.

## Compresseur Vector3

Nous n'avons pas pu trouver une manière plus optimise que d'encoder les 3 float a la suite pour sérialiser un vector3. Les fichiers (cpp et hpp) de la classe sont encore dans le dossier du projet si vous souhaitez y jeter un œil, mais ils ne sont pas utilisés dans l'exécutable.

## Dirtiness

Par manque de temps, nous n'avons pas pu implémenter la non-sérialisation de la coordonnée z si jamais elle n'a pas changé.

## Librairie Boost

La librairie boost a été intégrée pour pouvoir utiliser dynamic_bitset qui permet de créer des suites de bits, mais sans préciser le nombre de bit qu'elle contient, contrairement à std::bitset. Des dynamic_bitset ont été utilisé pour la compression.
